
#include <zapata/chain/zapata_object_types.hpp>

#include <zapata/chain/index.hpp>

#include <zapata/chain/block_summary_object.hpp>
#include <zapata/chain/history_object.hpp>
#include <zapata/chain/zapata_objects.hpp>
#include <zapata/chain/sps_objects.hpp>
#include <zapata/chain/transaction_object.hpp>
#include <zapata/chain/witness_schedule.hpp>

namespace zapata { namespace chain {

void initialize_core_indexes( database& db )
{
   ZAPATA_ADD_CORE_INDEX(db, dynamic_global_property_index);
   ZAPATA_ADD_CORE_INDEX(db, account_index);
   ZAPATA_ADD_CORE_INDEX(db, account_metadata_index);
   ZAPATA_ADD_CORE_INDEX(db, account_authority_index);
   ZAPATA_ADD_CORE_INDEX(db, witness_index);
   ZAPATA_ADD_CORE_INDEX(db, transaction_index);
   ZAPATA_ADD_CORE_INDEX(db, block_summary_index);
   ZAPATA_ADD_CORE_INDEX(db, witness_schedule_index);
   ZAPATA_ADD_CORE_INDEX(db, comment_index);
   ZAPATA_ADD_CORE_INDEX(db, comment_content_index);
   ZAPATA_ADD_CORE_INDEX(db, comment_vote_index);
   ZAPATA_ADD_CORE_INDEX(db, witness_vote_index);
   ZAPATA_ADD_CORE_INDEX(db, operation_index);
   ZAPATA_ADD_CORE_INDEX(db, account_history_index);
   ZAPATA_ADD_CORE_INDEX(db, hardfork_property_index);
   ZAPATA_ADD_CORE_INDEX(db, withdraw_vesting_route_index);
   ZAPATA_ADD_CORE_INDEX(db, owner_authority_history_index);
   ZAPATA_ADD_CORE_INDEX(db, account_recovery_request_index);
   ZAPATA_ADD_CORE_INDEX(db, change_recovery_account_request_index);
   ZAPATA_ADD_CORE_INDEX(db, escrow_index);
   ZAPATA_ADD_CORE_INDEX(db, savings_withdraw_index);
   ZAPATA_ADD_CORE_INDEX(db, decline_voting_rights_request_index);
   ZAPATA_ADD_CORE_INDEX(db, reward_fund_index);
   ZAPATA_ADD_CORE_INDEX(db, vesting_delegation_index);
   ZAPATA_ADD_CORE_INDEX(db, vesting_delegation_expiration_index);
   ZAPATA_ADD_CORE_INDEX(db, proposal_index);
   ZAPATA_ADD_CORE_INDEX(db, proposal_vote_index);
}

index_info::index_info() {}
index_info::~index_info() {}

} }


#pragma once

#include <memory>

#include <zapata/protocol/types.hpp>

namespace zapata { namespace schema {
   struct abstract_schema;
} }

namespace zapata { namespace protocol {
   struct custom_json_operation;
} }

namespace zapata { namespace chain {

class custom_operation_interpreter
{
   public:
      virtual void apply( const protocol::custom_json_operation& op ) = 0;
      virtual void apply( const protocol::custom_binary_operation & op ) = 0;
      virtual zapata::protocol::custom_id_type get_custom_id() = 0;
      virtual std::shared_ptr< zapata::schema::abstract_schema > get_operation_schema() = 0;
};

} } // zapata::chain

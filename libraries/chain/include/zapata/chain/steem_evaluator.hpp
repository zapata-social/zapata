#pragma once

#include <zapata/protocol/zapata_operations.hpp>

#include <zapata/chain/evaluator.hpp>

namespace zapata { namespace chain {

using namespace zapata::protocol;

ZAPATA_DEFINE_EVALUATOR( account_create )
ZAPATA_DEFINE_EVALUATOR( account_update )
ZAPATA_DEFINE_EVALUATOR( transfer )
ZAPATA_DEFINE_EVALUATOR( transfer_to_vesting )
ZAPATA_DEFINE_EVALUATOR( witness_update )
ZAPATA_DEFINE_EVALUATOR( account_witness_vote )
ZAPATA_DEFINE_EVALUATOR( account_witness_proxy )
ZAPATA_DEFINE_EVALUATOR( withdraw_vesting )
ZAPATA_DEFINE_EVALUATOR( set_withdraw_vesting_route )
ZAPATA_DEFINE_EVALUATOR( comment )
ZAPATA_DEFINE_EVALUATOR( comment_options )
ZAPATA_DEFINE_EVALUATOR( delete_comment )
ZAPATA_DEFINE_EVALUATOR( vote )
ZAPATA_DEFINE_EVALUATOR( custom )
ZAPATA_DEFINE_EVALUATOR( custom_json )
ZAPATA_DEFINE_EVALUATOR( custom_binary )
ZAPATA_DEFINE_EVALUATOR( escrow_transfer )
ZAPATA_DEFINE_EVALUATOR( escrow_approve )
ZAPATA_DEFINE_EVALUATOR( escrow_dispute )
ZAPATA_DEFINE_EVALUATOR( escrow_release )
ZAPATA_DEFINE_EVALUATOR( claim_account )
ZAPATA_DEFINE_EVALUATOR( create_claimed_account )
ZAPATA_DEFINE_EVALUATOR( request_account_recovery )
ZAPATA_DEFINE_EVALUATOR( recover_account )
ZAPATA_DEFINE_EVALUATOR( change_recovery_account )
ZAPATA_DEFINE_EVALUATOR( transfer_to_savings )
ZAPATA_DEFINE_EVALUATOR( transfer_from_savings )
ZAPATA_DEFINE_EVALUATOR( cancel_transfer_from_savings )
ZAPATA_DEFINE_EVALUATOR( decline_voting_rights )
ZAPATA_DEFINE_EVALUATOR( reset_account )
ZAPATA_DEFINE_EVALUATOR( set_reset_account )
ZAPATA_DEFINE_EVALUATOR( claim_reward_balance )
ZAPATA_DEFINE_EVALUATOR( delegate_vesting_shares )
ZAPATA_DEFINE_EVALUATOR( witness_set_properties )
ZAPATA_DEFINE_EVALUATOR( create_proposal )
ZAPATA_DEFINE_EVALUATOR( update_proposal_votes )
ZAPATA_DEFINE_EVALUATOR( remove_proposal )

} } // zapata::chain

#pragma once

#include <zapata/protocol/block.hpp>

namespace zapata { namespace chain {

struct transaction_notification
{
   transaction_notification( const zapata::protocol::signed_transaction& tx ) : transaction(tx)
   {
      transaction_id = tx.id();
   }

   zapata::protocol::transaction_id_type          transaction_id;
   const zapata::protocol::signed_transaction&    transaction;
};

} }

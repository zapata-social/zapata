#pragma once

#include <zapata/protocol/block.hpp>

namespace zapata { namespace chain {

struct block_notification
{
   block_notification( const zapata::protocol::signed_block& b ) : block(b)
   {
      block_id = b.id();
      block_num = zapata::protocol::block_header::num_from_id( block_id );
   }

   zapata::protocol::block_id_type          block_id;
   uint32_t                                block_num = 0;
   const zapata::protocol::signed_block&    block;
};

struct transaction_notification
{
   transaction_notification( const zapata::protocol::signed_transaction& tx ) : transaction(tx)
   {
      transaction_id = tx.id();
   }

   zapata::protocol::transaction_id_type          transaction_id;
   const zapata::protocol::signed_transaction&    transaction;
};

struct operation_notification
{
   operation_notification( const zapata::protocol::operation& o ) : op(o) {}

   transaction_id_type trx_id;
   uint32_t            block = 0;
   uint32_t            trx_in_block = 0;
   uint32_t            op_in_trx = 0;
   uint32_t            virtual_op = 0;
   const zapata::protocol::operation&    op;
};

} }

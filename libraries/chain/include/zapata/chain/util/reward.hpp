#pragma once

#include <zapata/protocol/asset.hpp>
#include <zapata/protocol/config.hpp>
#include <zapata/protocol/types.hpp>
#include <zapata/protocol/misc_utilities.hpp>

#include <fc/reflect/reflect.hpp>

#include <fc/uint128.hpp>

namespace zapata { namespace chain { namespace util {

using zapata::protocol::asset;
using zapata::protocol::price;
using zapata::protocol::share_type;

using fc::uint128_t;

struct comment_reward_context
{
   share_type rshares;
   uint16_t   reward_weight = 0;
   asset      max_payout;
   uint128_t  total_reward_shares2;
   asset      total_reward_fund_zapata;
   protocol::curve_id   reward_curve = protocol::quadratic;
   uint128_t  content_constant = ZAPATA_CONTENT_CONSTANT_HF21;
};

uint64_t get_rshare_reward( const comment_reward_context& ctx );

uint128_t evaluate_reward_curve( const uint128_t& rshares, const protocol::curve_id& curve = protocol::quadratic, const uint128_t& var1 = ZAPATA_CONTENT_CONSTANT_HF21 );

inline bool is_comment_payout_dust( uint64_t zapata_payout )
{
   return asset( zapata_payout, ZAPATA_SYMBOL ) < ZAPATA_MIN_PAYOUT;
}

} } } // zapata::chain::util

FC_REFLECT( zapata::chain::util::comment_reward_context,
   (rshares)
   (reward_weight)
   (max_payout)
   (total_reward_shares2)
   (total_reward_fund_zapata)
   (reward_curve)
   (content_constant)
   )

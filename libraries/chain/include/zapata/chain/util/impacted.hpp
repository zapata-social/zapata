#pragma once

#include <fc/container/flat.hpp>
#include <zapata/protocol/operations.hpp>
#include <zapata/protocol/transaction.hpp>

#include <fc/string.hpp>

namespace zapata { namespace app {

using namespace fc;

void operation_get_impacted_accounts(
   const zapata::protocol::operation& op,
   fc::flat_set<protocol::account_name_type>& result );

void transaction_get_impacted_accounts(
   const zapata::protocol::transaction& tx,
   fc::flat_set<protocol::account_name_type>& result
   );

} } // zapata::app

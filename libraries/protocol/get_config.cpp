#include <zapata/protocol/get_config.hpp>
#include <zapata/protocol/config.hpp>
#include <zapata/protocol/asset.hpp>
#include <zapata/protocol/types.hpp>
#include <zapata/protocol/version.hpp>

namespace zapata { namespace protocol {

fc::variant_object get_config()
{
   fc::mutable_variant_object result;

#ifdef IS_TEST_NET
   result[ "IS_TEST_NET" ] = true;
#else
   result[ "IS_TEST_NET" ] = false;
#endif
   result["ZAPATA_REDUCED_VOTE_POWER_RATE"] = ZAPATA_REDUCED_VOTE_POWER_RATE;
   result["ZAPATA_100_PERCENT"] = ZAPATA_100_PERCENT;
   result["ZAPATA_1_PERCENT"] = ZAPATA_1_PERCENT;
   result["ZAPATA_ACCOUNT_RECOVERY_REQUEST_EXPIRATION_PERIOD"] = ZAPATA_ACCOUNT_RECOVERY_REQUEST_EXPIRATION_PERIOD;
   result["ZAPATA_ACTIVE_CHALLENGE_COOLDOWN"] = ZAPATA_ACTIVE_CHALLENGE_COOLDOWN;
   result["ZAPATA_ACTIVE_CHALLENGE_FEE"] = ZAPATA_ACTIVE_CHALLENGE_FEE;
   result["ZAPATA_ADDRESS_PREFIX"] = ZAPATA_ADDRESS_PREFIX;
   result["ZAPATA_APR_PERCENT_MULTIPLY_PER_BLOCK"] = ZAPATA_APR_PERCENT_MULTIPLY_PER_BLOCK;
   result["ZAPATA_APR_PERCENT_MULTIPLY_PER_HOUR"] = ZAPATA_APR_PERCENT_MULTIPLY_PER_HOUR;
   result["ZAPATA_APR_PERCENT_MULTIPLY_PER_ROUND"] = ZAPATA_APR_PERCENT_MULTIPLY_PER_ROUND;
   result["ZAPATA_APR_PERCENT_SHIFT_PER_BLOCK"] = ZAPATA_APR_PERCENT_SHIFT_PER_BLOCK;
   result["ZAPATA_APR_PERCENT_SHIFT_PER_HOUR"] = ZAPATA_APR_PERCENT_SHIFT_PER_HOUR;
   result["ZAPATA_APR_PERCENT_SHIFT_PER_ROUND"] = ZAPATA_APR_PERCENT_SHIFT_PER_ROUND;
   result["ZAPATA_BANDWIDTH_AVERAGE_WINDOW_SECONDS"] = ZAPATA_BANDWIDTH_AVERAGE_WINDOW_SECONDS;
   result["ZAPATA_BANDWIDTH_PRECISION"] = ZAPATA_BANDWIDTH_PRECISION;
   result["ZAPATA_BENEFICIARY_LIMIT"] = ZAPATA_BENEFICIARY_LIMIT;
   result["ZAPATA_BLOCKCHAIN_PRECISION"] = ZAPATA_BLOCKCHAIN_PRECISION;
   result["ZAPATA_BLOCKCHAIN_PRECISION_DIGITS"] = ZAPATA_BLOCKCHAIN_PRECISION_DIGITS;
   result["ZAPATA_BLOCKCHAIN_HARDFORK_VERSION"] = ZAPATA_BLOCKCHAIN_HARDFORK_VERSION;
   result["ZAPATA_BLOCKCHAIN_VERSION"] = ZAPATA_BLOCKCHAIN_VERSION;
   result["ZAPATA_BLOCK_INTERVAL"] = ZAPATA_BLOCK_INTERVAL;
   result["ZAPATA_BLOCKS_PER_DAY"] = ZAPATA_BLOCKS_PER_DAY;
   result["ZAPATA_BLOCKS_PER_HOUR"] = ZAPATA_BLOCKS_PER_HOUR;
   result["ZAPATA_BLOCKS_PER_YEAR"] = ZAPATA_BLOCKS_PER_YEAR;
   result["ZAPATA_CASHOUT_WINDOW_SECONDS"] = ZAPATA_CASHOUT_WINDOW_SECONDS;
   result["ZAPATA_CHAIN_ID"] = ZAPATA_CHAIN_ID;
   result["ZAPATA_COMMENT_TITLE_LIMIT"] = ZAPATA_COMMENT_TITLE_LIMIT;
   result["ZAPATA_CONTENT_CONSTANT_HF21"] = ZAPATA_CONTENT_CONSTANT_HF21;
   result["ZAPATA_CONTENT_REWARD_PERCENT_HF21"] = ZAPATA_CONTENT_REWARD_PERCENT_HF21;
   result["ZAPATA_CUSTOM_OP_DATA_MAX_LENGTH"] = ZAPATA_CUSTOM_OP_DATA_MAX_LENGTH;
   result["ZAPATA_CUSTOM_OP_ID_MAX_LENGTH"] = ZAPATA_CUSTOM_OP_ID_MAX_LENGTH;
   result["ZAPATA_GENESIS_TIME"] = ZAPATA_GENESIS_TIME;
   result["ZAPATA_HARDFORK_REQUIRED_WITNESSES"] = ZAPATA_HARDFORK_REQUIRED_WITNESSES;
   result["ZAPATA_HF21_CONVERGENT_LINEAR_RECENT_CLAIMS"] = ZAPATA_HF21_CONVERGENT_LINEAR_RECENT_CLAIMS;
   result["ZAPATA_INFLATION_NARROWING_PERIOD"] = ZAPATA_INFLATION_NARROWING_PERIOD;
   result["ZAPATA_INFLATION_RATE_START_PERCENT"] = ZAPATA_INFLATION_RATE_START_PERCENT;
   result["ZAPATA_INFLATION_RATE_STOP_PERCENT"] = ZAPATA_INFLATION_RATE_STOP_PERCENT;
   result["ZAPATA_INIT_MINER_NAME"] = ZAPATA_INIT_MINER_NAME;
   result["ZAPATA_INIT_PUBLIC_KEY_STR"] = ZAPATA_INIT_PUBLIC_KEY_STR;
#if 0
   // do not expose private key, period.
   // we need this line present but inactivated so CI check for all constants in config.hpp doesn't complain.
   result["ZAPATA_INIT_PRIVATE_KEY"] = ZAPATA_INIT_PRIVATE_KEY;
#endif
   result["ZAPATA_INIT_SUPPLY"] = ZAPATA_INIT_SUPPLY;
   result["ZAPATA_IRREVERSIBLE_THRESHOLD"] = ZAPATA_IRREVERSIBLE_THRESHOLD;
   result["ZAPATA_MAX_ACCOUNT_CREATION_FEE"] = ZAPATA_MAX_ACCOUNT_CREATION_FEE;
   result["ZAPATA_MAX_ACCOUNT_NAME_LENGTH"] = ZAPATA_MAX_ACCOUNT_NAME_LENGTH;
   result["ZAPATA_MAX_ACCOUNT_WITNESS_VOTES"] = ZAPATA_MAX_ACCOUNT_WITNESS_VOTES;
   result["ZAPATA_MAX_ASSET_WHITELIST_AUTHORITIES"] = ZAPATA_MAX_ASSET_WHITELIST_AUTHORITIES;
   result["ZAPATA_MAX_AUTHORITY_MEMBERSHIP"] = ZAPATA_MAX_AUTHORITY_MEMBERSHIP;
   result["ZAPATA_MAX_BLOCK_SIZE"] = ZAPATA_MAX_BLOCK_SIZE;
   result["ZAPATA_SOFT_MAX_BLOCK_SIZE"] = ZAPATA_SOFT_MAX_BLOCK_SIZE;
   result["ZAPATA_MAX_CASHOUT_WINDOW_SECONDS"] = ZAPATA_MAX_CASHOUT_WINDOW_SECONDS;
   result["ZAPATA_MAX_COMMENT_DEPTH"] = ZAPATA_MAX_COMMENT_DEPTH;
   result["ZAPATA_MAX_INSTANCE_ID"] = ZAPATA_MAX_INSTANCE_ID;
   result["ZAPATA_MAX_MEMO_SIZE"] = ZAPATA_MAX_MEMO_SIZE;
   result["ZAPATA_MAX_WITNESSES"] = ZAPATA_MAX_WITNESSES;
   result["ZAPATA_MAX_PERMLINK_LENGTH"] = ZAPATA_MAX_PERMLINK_LENGTH;
   result["ZAPATA_MAX_PROXY_RECURSION_DEPTH"] = ZAPATA_MAX_PROXY_RECURSION_DEPTH;
   result["ZAPATA_MAX_RUNNER_WITNESSES_HF17"] = ZAPATA_MAX_RUNNER_WITNESSES_HF17;
   result["ZAPATA_MAX_SATOSHIS"] = ZAPATA_MAX_SATOSHIS;
   result["ZAPATA_MAX_SHARE_SUPPLY"] = ZAPATA_MAX_SHARE_SUPPLY;
   result["ZAPATA_MAX_SIG_CHECK_DEPTH"] = ZAPATA_MAX_SIG_CHECK_DEPTH;
   result["ZAPATA_MAX_SIG_CHECK_ACCOUNTS"] = ZAPATA_MAX_SIG_CHECK_ACCOUNTS;
   result["ZAPATA_MAX_TIME_UNTIL_EXPIRATION"] = ZAPATA_MAX_TIME_UNTIL_EXPIRATION;
   result["ZAPATA_MAX_TRANSACTION_SIZE"] = ZAPATA_MAX_TRANSACTION_SIZE;
   result["ZAPATA_MAX_UNDO_HISTORY"] = ZAPATA_MAX_UNDO_HISTORY;
   result["ZAPATA_MAX_URL_LENGTH"] = ZAPATA_MAX_URL_LENGTH;
   result["ZAPATA_MAX_VOTE_CHANGES"] = ZAPATA_MAX_VOTE_CHANGES;
   result["ZAPATA_MAX_VOTED_WITNESSES_HF17"] = ZAPATA_MAX_VOTED_WITNESSES_HF17;
   result["ZAPATA_MAX_WITHDRAW_ROUTES"] = ZAPATA_MAX_WITHDRAW_ROUTES;
   result["ZAPATA_MAX_WITNESS_URL_LENGTH"] = ZAPATA_MAX_WITNESS_URL_LENGTH;
   result["ZAPATA_MIN_ACCOUNT_CREATION_FEE"] = ZAPATA_MIN_ACCOUNT_CREATION_FEE;
   result["ZAPATA_MIN_ACCOUNT_NAME_LENGTH"] = ZAPATA_MIN_ACCOUNT_NAME_LENGTH;
   result["ZAPATA_MIN_BLOCK_SIZE_LIMIT"] = ZAPATA_MIN_BLOCK_SIZE_LIMIT;
   result["ZAPATA_MIN_BLOCK_SIZE"] = ZAPATA_MIN_BLOCK_SIZE;
   result["ZAPATA_MIN_CONTENT_REWARD"] = ZAPATA_MIN_CONTENT_REWARD;
   result["ZAPATA_MIN_CURATE_REWARD"] = ZAPATA_MIN_CURATE_REWARD;
   result["ZAPATA_MIN_PERMLINK_LENGTH"] = ZAPATA_MIN_PERMLINK_LENGTH;
   result["ZAPATA_MIN_REPLY_INTERVAL_HF20"] = ZAPATA_MIN_REPLY_INTERVAL_HF20;
   result["ZAPATA_MIN_ROOT_COMMENT_INTERVAL"] = ZAPATA_MIN_ROOT_COMMENT_INTERVAL;
   result["ZAPATA_MIN_COMMENT_EDIT_INTERVAL"] = ZAPATA_MIN_COMMENT_EDIT_INTERVAL;
   result["ZAPATA_MIN_VOTE_INTERVAL_SEC"] = ZAPATA_MIN_VOTE_INTERVAL_SEC;
   result["ZAPATA_MINER_ACCOUNT"] = ZAPATA_MINER_ACCOUNT;
   result["ZAPATA_MIN_PAYOUT"] = ZAPATA_MIN_PAYOUT;
   result["ZAPATA_MIN_PRODUCER_REWARD"] = ZAPATA_MIN_PRODUCER_REWARD;
   result["ZAPATA_MIN_TRANSACTION_EXPIRATION_LIMIT"] = ZAPATA_MIN_TRANSACTION_EXPIRATION_LIMIT;
   result["ZAPATA_MIN_TRANSACTION_SIZE_LIMIT"] = ZAPATA_MIN_TRANSACTION_SIZE_LIMIT;
   result["ZAPATA_MIN_UNDO_HISTORY"] = ZAPATA_MIN_UNDO_HISTORY;
   result["ZAPATA_NULL_ACCOUNT"] = ZAPATA_NULL_ACCOUNT;
   result["ZAPATA_OWNER_AUTH_RECOVERY_PERIOD"] = ZAPATA_OWNER_AUTH_RECOVERY_PERIOD;
   result["ZAPATA_OWNER_CHALLENGE_COOLDOWN"] = ZAPATA_OWNER_CHALLENGE_COOLDOWN;
   result["ZAPATA_OWNER_CHALLENGE_FEE"] = ZAPATA_OWNER_CHALLENGE_FEE;
   result["ZAPATA_OWNER_UPDATE_LIMIT"] = ZAPATA_OWNER_UPDATE_LIMIT;
   result["ZAPATA_POST_REWARD_FUND_NAME"] = ZAPATA_POST_REWARD_FUND_NAME;
   result["ZAPATA_PROXY_TO_SELF_ACCOUNT"] = ZAPATA_PROXY_TO_SELF_ACCOUNT;
   result["ZAPATA_SECONDS_PER_YEAR"] = ZAPATA_SECONDS_PER_YEAR;
   result["ZAPATA_PROPOSAL_FUND_PERCENT_HF21"] = ZAPATA_PROPOSAL_FUND_PERCENT_HF21;
   result["ZAPATA_RECENT_RSHARES_DECAY_TIME_HF19"] = ZAPATA_RECENT_RSHARES_DECAY_TIME_HF19;
   result["ZAPATA_REVERSE_AUCTION_WINDOW_SECONDS_HF21"] = ZAPATA_REVERSE_AUCTION_WINDOW_SECONDS_HF21;
   result["ZAPATA_ROOT_POST_PARENT"] = ZAPATA_ROOT_POST_PARENT;
   result["ZAPATA_SAVINGS_WITHDRAW_REQUEST_LIMIT"] = ZAPATA_SAVINGS_WITHDRAW_REQUEST_LIMIT;
   result["ZAPATA_SAVINGS_WITHDRAW_TIME"] = ZAPATA_SAVINGS_WITHDRAW_TIME;
   result["ZAPATA_SOFT_MAX_COMMENT_DEPTH"] = ZAPATA_SOFT_MAX_COMMENT_DEPTH;
   result["ZAPATA_TEMP_ACCOUNT"] = ZAPATA_TEMP_ACCOUNT;
   result["ZAPATA_UPVOTE_LOCKOUT_HF17"] = ZAPATA_UPVOTE_LOCKOUT_HF17;
   result["ZAPATA_UPVOTE_LOCKOUT_SECONDS"] = ZAPATA_UPVOTE_LOCKOUT_SECONDS;
   result["ZAPATA_VESTING_FUND_PERCENT_HF16"] = ZAPATA_VESTING_FUND_PERCENT_HF16;
   result["ZAPATA_VESTING_WITHDRAW_INTERVALS"] = ZAPATA_VESTING_WITHDRAW_INTERVALS;
   result["ZAPATA_VESTING_WITHDRAW_INTERVAL_SECONDS"] = ZAPATA_VESTING_WITHDRAW_INTERVAL_SECONDS;
   result["ZAPATA_VOTE_DUST_THRESHOLD"] = ZAPATA_VOTE_DUST_THRESHOLD;
   result["ZAPATA_VOTING_MANA_REGENERATION_SECONDS"] = ZAPATA_VOTING_MANA_REGENERATION_SECONDS;
   result["ZAPATA_SYMBOL"] = ZAPATA_SYMBOL;
   result["VESTS_SYMBOL"] = VESTS_SYMBOL;
   result["ZAPATA_VIRTUAL_SCHEDULE_LAP_LENGTH2"] = ZAPATA_VIRTUAL_SCHEDULE_LAP_LENGTH2;
   result["ZAPATA_DELEGATION_RETURN_PERIOD_HF20"] = ZAPATA_DELEGATION_RETURN_PERIOD_HF20;
   result["ZAPATA_RD_MIN_DECAY_BITS"] = ZAPATA_RD_MIN_DECAY_BITS;
   result["ZAPATA_RD_MAX_DECAY_BITS"] = ZAPATA_RD_MAX_DECAY_BITS;
   result["ZAPATA_RD_DECAY_DENOM_SHIFT"] = ZAPATA_RD_DECAY_DENOM_SHIFT;
   result["ZAPATA_RD_MAX_POOL_BITS"] = ZAPATA_RD_MAX_POOL_BITS;
   result["ZAPATA_RD_MAX_BUDGET_1"] = ZAPATA_RD_MAX_BUDGET_1;
   result["ZAPATA_RD_MAX_BUDGET_2"] = ZAPATA_RD_MAX_BUDGET_2;
   result["ZAPATA_RD_MAX_BUDGET_3"] = ZAPATA_RD_MAX_BUDGET_3;
   result["ZAPATA_RD_MAX_BUDGET"] = ZAPATA_RD_MAX_BUDGET;
   result["ZAPATA_RD_MIN_DECAY"] = ZAPATA_RD_MIN_DECAY;
   result["ZAPATA_RD_MIN_BUDGET"] = ZAPATA_RD_MIN_BUDGET;
   result["ZAPATA_RD_MAX_DECAY"] = ZAPATA_RD_MAX_DECAY;
   result["ZAPATA_ACCOUNT_SUBSIDY_PRECISION"] = ZAPATA_ACCOUNT_SUBSIDY_PRECISION;
   result["ZAPATA_WITNESS_SUBSIDY_BUDGET_PERCENT"] = ZAPATA_WITNESS_SUBSIDY_BUDGET_PERCENT;
   result["ZAPATA_WITNESS_SUBSIDY_DECAY_PERCENT"] = ZAPATA_WITNESS_SUBSIDY_DECAY_PERCENT;
   result["ZAPATA_DEFAULT_ACCOUNT_SUBSIDY_DECAY"] = ZAPATA_DEFAULT_ACCOUNT_SUBSIDY_DECAY;
   result["ZAPATA_DEFAULT_ACCOUNT_SUBSIDY_BUDGET"] = ZAPATA_DEFAULT_ACCOUNT_SUBSIDY_BUDGET;
   result["ZAPATA_DECAY_BACKSTOP_PERCENT"] = ZAPATA_DECAY_BACKSTOP_PERCENT;
   result["ZAPATA_BLOCK_GENERATION_POSTPONED_TX_LIMIT"] = ZAPATA_BLOCK_GENERATION_POSTPONED_TX_LIMIT;
   result["ZAPATA_PENDING_TRANSACTION_EXECUTION_LIMIT"] = ZAPATA_PENDING_TRANSACTION_EXECUTION_LIMIT;
   result["ZAPATA_TREASURY_ACCOUNT"] = ZAPATA_TREASURY_ACCOUNT;
   result["ZAPATA_TREASURY_FEE"] = ZAPATA_TREASURY_FEE;
   result["ZAPATA_PROPOSAL_MAINTENANCE_PERIOD"] = ZAPATA_PROPOSAL_MAINTENANCE_PERIOD;
   result["ZAPATA_PROPOSAL_MAINTENANCE_CLEANUP"] = ZAPATA_PROPOSAL_MAINTENANCE_CLEANUP;
   result["ZAPATA_PROPOSAL_SUBJECT_MAX_LENGTH"] = ZAPATA_PROPOSAL_SUBJECT_MAX_LENGTH;
   result["ZAPATA_PROPOSAL_MAX_IDS_NUMBER"] = ZAPATA_PROPOSAL_MAX_IDS_NUMBER;
   result["ZAPATA_INIT_POST_REWARD_BALANCE"] = ZAPATA_INIT_POST_REWARD_BALANCE;

   return result;
}

} } // zapata::protocol

#pragma once

namespace zapata { namespace protocol {

enum curve_id
{
   quadratic,
   bounded_curation,
   linear,
   square_root,
   convergent_linear,
   convergent_square_root
};

} } // zapata::utilities


FC_REFLECT_ENUM(
   zapata::protocol::curve_id,
   (quadratic)
   (bounded_curation)
   (linear)
   (square_root)
   (convergent_linear)
   (convergent_square_root)
)

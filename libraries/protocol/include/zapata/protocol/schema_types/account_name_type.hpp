
#pragma once

#include <zapata/schema/abstract_schema.hpp>
#include <zapata/schema/schema_impl.hpp>

#include <zapata/protocol/types.hpp>

namespace zapata { namespace schema { namespace detail {

//////////////////////////////////////////////
// account_name_type                        //
//////////////////////////////////////////////

struct schema_account_name_type_impl
   : public abstract_schema
{
   ZAPATA_SCHEMA_CLASS_BODY( schema_account_name_type_impl )
};

}

template<>
struct schema_reflect< zapata::protocol::account_name_type >
{
   typedef detail::schema_account_name_type_impl           schema_impl_type;
};

} }

namespace fc {

template<>
struct get_typename< zapata::protocol::account_name_type >
{
   static const char* name()
   {
      return "zapata::protocol::account_name_type";
   }
};

}


#pragma once

#include <zapata/schema/abstract_schema.hpp>
#include <zapata/schema/schema_impl.hpp>

#include <zapata/protocol/asset_symbol.hpp>

namespace zapata { namespace schema { namespace detail {

//////////////////////////////////////////////
// asset_symbol_type                        //
//////////////////////////////////////////////

struct schema_asset_symbol_type_impl
   : public abstract_schema
{
   ZAPATA_SCHEMA_CLASS_BODY( schema_asset_symbol_type_impl )
};

}

template<>
struct schema_reflect< zapata::protocol::asset_symbol_type >
{
   typedef detail::schema_asset_symbol_type_impl           schema_impl_type;
};

} }

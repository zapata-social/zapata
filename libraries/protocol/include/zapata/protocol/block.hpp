#pragma once
#include <zapata/protocol/block_header.hpp>
#include <zapata/protocol/transaction.hpp>

namespace zapata { namespace protocol {

   struct signed_block : public signed_block_header
   {
      checksum_type calculate_merkle_root()const;
      vector<signed_transaction> transactions;
   };

} } // zapata::protocol

FC_REFLECT_DERIVED( zapata::protocol::signed_block, (zapata::protocol::signed_block_header), (transactions) )

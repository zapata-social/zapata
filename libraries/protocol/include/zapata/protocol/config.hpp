/*
 * Copyright (c) 2016 Steemit, Inc., and contributors.
 */
#pragma once
#include <zapata/protocol/hardfork.hpp>

// WARNING!
// Every symbol defined here needs to be handled appropriately in get_config.cpp
// This is checked by get_config_check.sh called from Dockerfile

#ifdef IS_TEST_NET
#define ZAPATA_BLOCKCHAIN_VERSION              ( version(0, 0, 0) )

#define ZAPATA_INIT_PRIVATE_KEY                (fc::ecc::private_key::regenerate(fc::sha256::hash(std::string("init_key"))))
#define ZAPATA_INIT_PUBLIC_KEY_STR             (std::string( zapata::protocol::public_key_type(ZAPATA_INIT_PRIVATE_KEY.get_public_key()) ))
#define ZAPATA_CHAIN_ID                        (fc::sha256::hash("testnet"))
#define ZAPATA_ADDRESS_PREFIX                  "TST"

#define ZAPATA_GENESIS_TIME                    (fc::time_point_sec(1591025652))
#define ZAPATA_CASHOUT_WINDOW_SECONDS          (60*60) /// 1 hr
#define ZAPATA_MAX_CASHOUT_WINDOW_SECONDS      (60*60*24) /// 1 day
#define ZAPATA_UPVOTE_LOCKOUT_SECONDS          (60*5)    /// 5 minutes
#define ZAPATA_UPVOTE_LOCKOUT_HF17             (fc::minutes(5))


#define ZAPATA_MIN_ACCOUNT_CREATION_FEE          0
#define ZAPATA_MAX_ACCOUNT_CREATION_FEE          int64_t(1000000000)

#define ZAPATA_OWNER_AUTH_RECOVERY_PERIOD                  fc::seconds(60)
#define ZAPATA_ACCOUNT_RECOVERY_REQUEST_EXPIRATION_PERIOD  fc::seconds(12)
#define ZAPATA_OWNER_UPDATE_LIMIT                          fc::seconds(0)

#define ZAPATA_VOTE_DUST_THRESHOLD             (0)

#define ZAPATA_INIT_MINER_NAME                 "initminer"

#else // IS LIVE ZAPA NETWORK

#define ZAPATA_BLOCKCHAIN_VERSION              ( version(0, 0, 0) )

#define ZAPATA_INIT_PUBLIC_KEY_STR             "ZAP875YGJ2rXwEhUr4hRXduZguwJKEJufsS4oYT6ehHWiDhev7hah"
#define ZAPATA_CHAIN_ID                        (fc::sha256::hash("zapata-testbed")) // afa547e2e0adef898219d7f74abf36e6e957460a70120db21a03fed76cc0e8b6
#define ZAPATA_ADDRESS_PREFIX                  "ZAP"

#define ZAPATA_GENESIS_TIME                    (fc::time_point_sec(1591025652))
#define ZAPATA_CASHOUT_WINDOW_SECONDS          (60*60*24*7)  /// 7 days
#define ZAPATA_MAX_CASHOUT_WINDOW_SECONDS      (60*60*24*14) /// 2 weeks
#define ZAPATA_UPVOTE_LOCKOUT_SECONDS          (60*60*12)    /// 12 hours
#define ZAPATA_UPVOTE_LOCKOUT_HF17             (fc::hours(12))

#define ZAPATA_MIN_ACCOUNT_CREATION_FEE           1
#define ZAPATA_MAX_ACCOUNT_CREATION_FEE           int64_t(1000000000)

#define ZAPATA_OWNER_AUTH_RECOVERY_PERIOD                  fc::days(30)
#define ZAPATA_ACCOUNT_RECOVERY_REQUEST_EXPIRATION_PERIOD  fc::days(1)
#define ZAPATA_OWNER_UPDATE_LIMIT                          fc::minutes(60)

#define ZAPATA_VOTE_DUST_THRESHOLD             (50000000)

#define ZAPATA_INIT_MINER_NAME                 "initzapata"

#endif

#define ZAPATA_INIT_SUPPLY                     int64_t(388031775139)

#define VESTS_SYMBOL  (zapata::protocol::asset_symbol_type::from_asset_num( ZAPATA_ASSET_NUM_VESTS ) )
#define ZAPATA_SYMBOL  (zapata::protocol::asset_symbol_type::from_asset_num( ZAPATA_ASSET_NUM_ZAPATA ) )

#define ZAPATA_BLOCKCHAIN_HARDFORK_VERSION     ( hardfork_version( ZAPATA_BLOCKCHAIN_VERSION ) )

#define ZAPATA_100_PERCENT                     10000
#define ZAPATA_1_PERCENT                       (ZAPATA_100_PERCENT/100)

#define ZAPATA_BLOCK_INTERVAL                  3
#define ZAPATA_BLOCKS_PER_YEAR                 (365*24*60*60/ZAPATA_BLOCK_INTERVAL)
#define ZAPATA_BLOCKS_PER_DAY                  (24*60*60/ZAPATA_BLOCK_INTERVAL)

#define ZAPATA_MAX_WITNESSES                   21
#define ZAPATA_MAX_VOTED_WITNESSES_HF17        20
#define ZAPATA_MAX_RUNNER_WITNESSES_HF17       1

#define ZAPATA_HARDFORK_REQUIRED_WITNESSES     17 // 17 of the 21 dpos witnesses (20 elected and 1 virtual time) required for hardfork. This guarantees 75% participation on all subsequent rounds.
#define ZAPATA_MAX_TIME_UNTIL_EXPIRATION       (60*60) // seconds,  aka: 1 hour
#define ZAPATA_MAX_MEMO_SIZE                   2048
#define ZAPATA_MAX_PROXY_RECURSION_DEPTH       4
#define ZAPATA_VESTING_WITHDRAW_INTERVALS      13
#define ZAPATA_VESTING_WITHDRAW_INTERVAL_SECONDS (60*60*24*7) /// 1 week per interval
#define ZAPATA_MAX_WITHDRAW_ROUTES             10
#define ZAPATA_SAVINGS_WITHDRAW_TIME        	(fc::days(3))
#define ZAPATA_SAVINGS_WITHDRAW_REQUEST_LIMIT  100
#define ZAPATA_VOTING_MANA_REGENERATION_SECONDS (5*60*60*24) // 5 day
#define ZAPATA_MAX_VOTE_CHANGES                5
#define ZAPATA_REVERSE_AUCTION_WINDOW_SECONDS_HF21 (60*5) /// 5 minutes
#define ZAPATA_MIN_VOTE_INTERVAL_SEC           3

#define ZAPATA_MIN_ROOT_COMMENT_INTERVAL       (fc::seconds(60*5)) // 5 minutes
#define ZAPATA_MIN_REPLY_INTERVAL_HF20         (fc::seconds(3)) // 3 seconds
#define ZAPATA_MIN_COMMENT_EDIT_INTERVAL       (fc::seconds(3)) // 3 seconds

#define ZAPATA_MAX_ACCOUNT_WITNESS_VOTES       30

#define ZAPATA_INFLATION_RATE_START_PERCENT    (978) // Fixes block 7,000,000 to 9.5%
#define ZAPATA_INFLATION_RATE_STOP_PERCENT     (95) // 0.95%
#define ZAPATA_INFLATION_NARROWING_PERIOD      (250000) // Narrow 0.01% every 250k blocks
#define ZAPATA_VESTING_FUND_PERCENT_HF16       (15*ZAPATA_1_PERCENT) //15% of inflation, 1.425% inflation
#define ZAPATA_CONTENT_REWARD_PERCENT_HF21     (65*ZAPATA_1_PERCENT)
#define ZAPATA_PROPOSAL_FUND_PERCENT_HF21      (10*ZAPATA_1_PERCENT)

#define ZAPATA_HF21_CONVERGENT_LINEAR_RECENT_CLAIMS (fc::uint128_t(0,305178654659033363ull))
#define ZAPATA_CONTENT_CONSTANT_HF21           (fc::uint128_t(0,2000000000000ull))

#define ZAPATA_BANDWIDTH_AVERAGE_WINDOW_SECONDS (60*60*24*7) ///< 1 week
#define ZAPATA_BANDWIDTH_PRECISION             (uint64_t(1000000)) ///< 1 million
#define ZAPATA_MAX_COMMENT_DEPTH               0xffff // 64k
#define ZAPATA_SOFT_MAX_COMMENT_DEPTH          0xff // 255

#define ZAPATA_MIN_CONTENT_REWARD              asset( 1000, ZAPATA_SYMBOL )
#define ZAPATA_MIN_CURATE_REWARD               asset( 1000, ZAPATA_SYMBOL )
#define ZAPATA_MIN_PRODUCER_REWARD             asset( 1000, ZAPATA_SYMBOL )

#define ZAPATA_ACTIVE_CHALLENGE_FEE            asset( 2000, ZAPATA_SYMBOL )
#define ZAPATA_OWNER_CHALLENGE_FEE             asset( 30000, ZAPATA_SYMBOL )
#define ZAPATA_ACTIVE_CHALLENGE_COOLDOWN       fc::days(1)
#define ZAPATA_OWNER_CHALLENGE_COOLDOWN        fc::days(1)

#define ZAPATA_POST_REWARD_FUND_NAME           ("post")
#define ZAPATA_RECENT_RSHARES_DECAY_TIME_HF19  (fc::days(15))
// note, if redefining these constants make sure calculate_claims doesn't overflow

// 5ccc e802 de5f
// int(expm1( log1p( 1 ) / BLOCKS_PER_YEAR ) * 2**ZAPATA_APR_PERCENT_SHIFT_PER_BLOCK / 100000 + 0.5)
// we use 100000 here instead of 10000 because we end up creating an additional 9x for vesting
#define ZAPATA_APR_PERCENT_MULTIPLY_PER_BLOCK          ( (uint64_t( 0x5ccc ) << 0x20) \
                                                        | (uint64_t( 0xe802 ) << 0x10) \
                                                        | (uint64_t( 0xde5f )        ) \
                                                        )
// chosen to be the maximal value such that ZAPATA_APR_PERCENT_MULTIPLY_PER_BLOCK * 2**64 * 100000 < 2**128
#define ZAPATA_APR_PERCENT_SHIFT_PER_BLOCK             87

#define ZAPATA_APR_PERCENT_MULTIPLY_PER_ROUND          ( (uint64_t( 0x79cc ) << 0x20 ) \
                                                        | (uint64_t( 0xf5c7 ) << 0x10 ) \
                                                        | (uint64_t( 0x3480 )         ) \
                                                        )

#define ZAPATA_APR_PERCENT_SHIFT_PER_ROUND             83

// We have different constants for hourly rewards
// i.e. hex(int(math.expm1( math.log1p( 1 ) / HOURS_PER_YEAR ) * 2**ZAPATA_APR_PERCENT_SHIFT_PER_HOUR / 100000 + 0.5))
#define ZAPATA_APR_PERCENT_MULTIPLY_PER_HOUR           ( (uint64_t( 0x6cc1 ) << 0x20) \
                                                        | (uint64_t( 0x39a1 ) << 0x10) \
                                                        | (uint64_t( 0x5cbd )        ) \
                                                        )

// chosen to be the maximal value such that ZAPATA_APR_PERCENT_MULTIPLY_PER_HOUR * 2**64 * 100000 < 2**128
#define ZAPATA_APR_PERCENT_SHIFT_PER_HOUR              77

#define ZAPATA_MIN_PAYOUT                      (asset(20,ZAPATA_SYMBOL))

#define ZAPATA_MIN_ACCOUNT_NAME_LENGTH          3
#define ZAPATA_MAX_ACCOUNT_NAME_LENGTH         16

#define ZAPATA_MIN_PERMLINK_LENGTH             0
#define ZAPATA_MAX_PERMLINK_LENGTH             256
#define ZAPATA_MAX_WITNESS_URL_LENGTH          2048

#define ZAPATA_MAX_SHARE_SUPPLY                int64_t(1000000000000000ll)
#define ZAPATA_MAX_SATOSHIS                    int64_t(4611686018427387903ll)
#define ZAPATA_MAX_SIG_CHECK_DEPTH             2
#define ZAPATA_MAX_SIG_CHECK_ACCOUNTS          125

#define ZAPATA_MIN_TRANSACTION_SIZE_LIMIT      1024
#define ZAPATA_SECONDS_PER_YEAR                (uint64_t(60*60*24*365ll))

#define ZAPATA_MAX_TRANSACTION_SIZE            (1024*64)
#define ZAPATA_MIN_BLOCK_SIZE_LIMIT            (ZAPATA_MAX_TRANSACTION_SIZE)
#define ZAPATA_MAX_BLOCK_SIZE                  (ZAPATA_MAX_TRANSACTION_SIZE*ZAPATA_BLOCK_INTERVAL*2000)
#define ZAPATA_SOFT_MAX_BLOCK_SIZE             (2*1024*1024)
#define ZAPATA_MIN_BLOCK_SIZE                  115
#define ZAPATA_BLOCKS_PER_HOUR                 (60*60/ZAPATA_BLOCK_INTERVAL)

#define ZAPATA_MIN_UNDO_HISTORY                10
#define ZAPATA_MAX_UNDO_HISTORY                10000

#define ZAPATA_MIN_TRANSACTION_EXPIRATION_LIMIT (ZAPATA_BLOCK_INTERVAL * 5) // 5 transactions per block
#define ZAPATA_BLOCKCHAIN_PRECISION            uint64_t( 1000 )

#define ZAPATA_BLOCKCHAIN_PRECISION_DIGITS     3
#define ZAPATA_MAX_INSTANCE_ID                 (uint64_t(-1)>>16)
/** NOTE: making this a power of 2 (say 2^15) would greatly accelerate fee calcs */
#define ZAPATA_MAX_AUTHORITY_MEMBERSHIP        40
#define ZAPATA_MAX_ASSET_WHITELIST_AUTHORITIES 10
#define ZAPATA_MAX_URL_LENGTH                  127

#define ZAPATA_IRREVERSIBLE_THRESHOLD          (75 * ZAPATA_1_PERCENT)

#define ZAPATA_VIRTUAL_SCHEDULE_LAP_LENGTH2 ( fc::uint128::max_value() )

#define ZAPATA_REDUCED_VOTE_POWER_RATE        (10)

#define ZAPATA_DELEGATION_RETURN_PERIOD_HF20  (ZAPATA_VOTING_MANA_REGENERATION_SECONDS)

#define ZAPATA_RD_MIN_DECAY_BITS               6
#define ZAPATA_RD_MAX_DECAY_BITS              32
#define ZAPATA_RD_DECAY_DENOM_SHIFT           36
#define ZAPATA_RD_MAX_POOL_BITS               64
#define ZAPATA_RD_MAX_BUDGET_1                ((uint64_t(1) << (ZAPATA_RD_MAX_POOL_BITS + ZAPATA_RD_MIN_DECAY_BITS - ZAPATA_RD_DECAY_DENOM_SHIFT))-1)
#define ZAPATA_RD_MAX_BUDGET_2                ((uint64_t(1) << (64-ZAPATA_RD_DECAY_DENOM_SHIFT))-1)
#define ZAPATA_RD_MAX_BUDGET_3                (uint64_t( std::numeric_limits<int32_t>::max() ))
#define ZAPATA_RD_MAX_BUDGET                  (int32_t( std::min( { ZAPATA_RD_MAX_BUDGET_1, ZAPATA_RD_MAX_BUDGET_2, ZAPATA_RD_MAX_BUDGET_3 } )) )
#define ZAPATA_RD_MIN_DECAY                   (uint32_t(1) << ZAPATA_RD_MIN_DECAY_BITS)
#define ZAPATA_RD_MIN_BUDGET                  1
#define ZAPATA_RD_MAX_DECAY                   (uint32_t(0xFFFFFFFF))

#define ZAPATA_ACCOUNT_SUBSIDY_PRECISION      (ZAPATA_100_PERCENT)

// We want the global subsidy to run out first in normal (Poisson)
// conditions, so we boost the per-witness subsidy a little.
#define ZAPATA_WITNESS_SUBSIDY_BUDGET_PERCENT (125 * ZAPATA_1_PERCENT)

// Since witness decay only procs once per round, multiplying the decay
// constant by the number of witnesses means the per-witness pools have
// the same effective decay rate in real-time terms.
#define ZAPATA_WITNESS_SUBSIDY_DECAY_PERCENT  (ZAPATA_MAX_WITNESSES * ZAPATA_100_PERCENT)

// 347321 corresponds to a 5-day halflife
#define ZAPATA_DEFAULT_ACCOUNT_SUBSIDY_DECAY  (347321)
// Default rate is 0.5 accounts per block
#define ZAPATA_DEFAULT_ACCOUNT_SUBSIDY_BUDGET (797)
#define ZAPATA_DECAY_BACKSTOP_PERCENT         (90 * ZAPATA_1_PERCENT)

#define ZAPATA_BLOCK_GENERATION_POSTPONED_TX_LIMIT 5
#define ZAPATA_PENDING_TRANSACTION_EXECUTION_LIMIT fc::milliseconds(200)

#define ZAPATA_CUSTOM_OP_ID_MAX_LENGTH        (32)
#define ZAPATA_CUSTOM_OP_DATA_MAX_LENGTH      (8192)
#define ZAPATA_BENEFICIARY_LIMIT              (128)
#define ZAPATA_COMMENT_TITLE_LIMIT            (256)

/**
 *  Reserved Account IDs with special meaning
 */
///@{
/// Represents the current witnesses
#define ZAPATA_MINER_ACCOUNT                   "miners"
/// Represents the canonical account with NO authority (nobody can access funds in null account)
#define ZAPATA_NULL_ACCOUNT                    "null"
/// Represents the canonical account with WILDCARD authority (anybody can access funds in temp account)
#define ZAPATA_TEMP_ACCOUNT                    "temp"
/// Represents the canonical account for specifying you will vote for directly (as opposed to a proxy)
#define ZAPATA_PROXY_TO_SELF_ACCOUNT           ""
/// Represents the canonical root post parent account
#define ZAPATA_ROOT_POST_PARENT                (account_name_type())
/// Represents the account with NO authority which holds resources for payouts according to given proposals
#define ZAPATA_TREASURY_ACCOUNT                "zapata.dao"
/// This regent account controls the chain within 2 years
#define ZAPATA_REGENT_ACCOUNT                  "zapata.regent"
///@}

/// ZAPATA PROPOSAL SYSTEM support

#define ZAPATA_TREASURY_FEE                         (10 * ZAPATA_BLOCKCHAIN_PRECISION)
#define ZAPATA_PROPOSAL_MAINTENANCE_PERIOD          3600
#define ZAPATA_PROPOSAL_MAINTENANCE_CLEANUP         (60*60*24*1) /// 1 day
#define ZAPATA_PROPOSAL_SUBJECT_MAX_LENGTH          80
/// Max number of IDs passed at once to the update_proposal_voter_operation or remove_proposal_operation.
#define ZAPATA_PROPOSAL_MAX_IDS_NUMBER              5


#define ZAPATA_INIT_POST_REWARD_BALANCE       int64_t(923210316)
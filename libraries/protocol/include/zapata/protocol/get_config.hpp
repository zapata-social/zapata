#pragma once

#include <fc/variant_object.hpp>

namespace zapata { namespace protocol {

fc::variant_object get_config();

} } // zapata::protocol

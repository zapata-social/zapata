
#pragma once

#include <zapata/schema/schema_types/flat_map.hpp>
#include <zapata/schema/schema_types/flat_set.hpp>
#include <zapata/schema/schema_types/static_variant.hpp>
#include <zapata/schema/schema_types/optional.hpp>
#include <zapata/schema/schema_types/pair.hpp>
#include <zapata/schema/schema_types/vector.hpp>

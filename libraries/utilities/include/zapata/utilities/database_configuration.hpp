#pragma once
#include <fc/variant.hpp>

namespace zapata { namespace utilities {

fc::variant default_database_configuration();

} } // zapata::utilities

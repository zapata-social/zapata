#include <zapata/plugins/follow/follow_operations.hpp>

#include <zapata/protocol/operation_util_impl.hpp>

namespace zapata { namespace plugins{ namespace follow {

void follow_operation::validate()const
{
   FC_ASSERT( follower != following, "You cannot follow yourself" );
}

void reblog_operation::validate()const
{
   FC_ASSERT( account != author, "You cannot reblog your own content" );
}

} } } //zapata::plugins::follow

ZAPATA_DEFINE_OPERATION_TYPE( zapata::plugins::follow::follow_plugin_operation )

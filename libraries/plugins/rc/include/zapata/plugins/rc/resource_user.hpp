#pragma once

#include <zapata/protocol/types.hpp>

#include <fc/reflect/reflect.hpp>

namespace zapata { namespace protocol {
struct signed_transaction;
} } // zapata::protocol

namespace zapata { namespace plugins { namespace rc {

using zapata::protocol::account_name_type;
using zapata::protocol::signed_transaction;

account_name_type get_resource_user( const signed_transaction& tx );

} } } // zapata::plugins::rc

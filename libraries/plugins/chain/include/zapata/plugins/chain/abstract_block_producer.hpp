#pragma once

#include <fc/time.hpp>

#include <zapata/chain/database.hpp>

namespace zapata { namespace plugins { namespace chain {
   
class abstract_block_producer {
public:
   virtual ~abstract_block_producer() = default;

   virtual zapata::chain::signed_block generate_block(
      fc::time_point_sec when,
      const zapata::chain::account_name_type& witness_owner,
      const fc::ecc::private_key& block_signing_private_key,
      uint32_t skip = zapata::chain::database::skip_nothing) = 0;
};

} } } // zapata::plugins::chain

#pragma once

#include <vector>

namespace zapata { namespace plugins { namespace p2p {

#ifdef IS_TEST_NET
const std::vector< std::string > default_seeds;
#else
const std::vector< std::string > default_seeds = {

};
#endif

} } } // zapata::plugins::p2p

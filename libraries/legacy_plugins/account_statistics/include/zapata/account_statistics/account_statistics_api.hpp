#pragma once

#include <zapata/account_statistics/account_statistics_plugin.hpp>

#include <fc/api.hpp>

namespace zapata { namespace app {
   struct api_context;
} }

namespace zapata { namespace account_statistics {

namespace detail
{
   class account_statistics_api_impl;
}

class account_statistics_api
{
   public:
      account_statistics_api( const zapata::app::api_context& ctx );

      void on_api_startup();

   private:
      std::shared_ptr< detail::account_statistics_api_impl > _my;
};

} } // zapata::account_statistics

FC_API( zapata::account_statistics::account_statistics_api, )
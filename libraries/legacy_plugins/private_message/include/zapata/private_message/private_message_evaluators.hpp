#pragma once

#include <zapata/chain/evaluator.hpp>

#include <zapata/private_message/private_message_operations.hpp>
#include <zapata/private_message/private_message_plugin.hpp>

namespace zapata { namespace private_message {

ZAPATA_DEFINE_PLUGIN_EVALUATOR( private_message_plugin, zapata::private_message::private_message_plugin_operation, private_message )

} }

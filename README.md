# Zapata

Zapata is a social blockchain hardfork/airdrop of Steem at block 43,526,969.



## Improvements

* Remove Downvotes.

* Remove SBD (Steem Backed Dollar).  



## Decentralization And Security

Zapata is powered by a Graphene-based implementation of the Delegated Proof-of-Stake Consensus (DPoS) consensus mechenism. 

Furthermore, Zapata introduces a novel regent account which has a special weighted vote to control the witnesses and flow of funds from the DAO for up to two years. 

The regent account is programmed to decay to zero over a two-year period with 24 equal monthly reductions, starting at a power equal to the total Zapata (ZAPA) supply.  <insert initial ZAPA supply>
 
The regent account does not impact:

- circulating supply, 
    
- new inflation A.K.A. organic supply, 
    
- the rewards pool. 



## Economy

* Ticker symbol ZAPA

* Staked ZAPA is called ZAPA POWER  (locked for 3 months)

* SBD has been removed and balances converted to their liquid ZAPA equivalent at the time of the snapshot. 

* 10% APR inflation narrowing to 1% APR over 20 years
    * 65% of inflation to Authors/Curators.
    * 15% of inflation to Stakeholders.
    * 10% of inflation to Zapata Witnesses.
    * 10% of inflation to Zapata DAO Fund.

    
    
## Specifications

Not Imported to Zapata From Steem: 

  * Content
  * Followers
  * Profile Pic
  * Name
  * Location
  * Pending Claimed Accounts



## Build

Do this exactly, and it'll work.  Do otherwise and you may experience difficulties.

Spin up a Debian 10 Virtual Machine with 16GB of RAM.  Then, run:

```
apt-get update
apt install git autoconf automake cmake g++ git libbz2-dev libsnappy-dev libssl-dev libtool make pkg-config python3 python3-jinja2 doxygen libboost-chrono-dev libboost-context-dev libboost-coroutine-dev libboost-date-time-dev libboost-filesystem-dev libboost-iostreams-dev libboost-locale-dev libboost-program-options-dev libboost-serialization-dev libboost-signals-dev libboost-system-dev libboost-test-dev libboost-thread-dev libncurses5-dev libreadline-dev perl
git clone https://gitlab.com/zapata/zapata
cd zapata
git submodule update --init --recursive
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j$(nproc) zapatad
make -j$(nproc) cli_wallet
```

#!/bin/bash

echo /tmp/core | tee /proc/sys/kernel/core_pattern
ulimit -c unlimited

# if we're not using PaaS mode then start zapatad traditionally with sv to control it
if [[ ! "$USE_PAAS" ]]; then
  mkdir -p /etc/service/zapatad
  cp /usr/local/bin/steem-sv-run.sh /etc/service/zapatad/run
  chmod +x /etc/service/zapatad/run
  runsv /etc/service/zapatad
elif [[ "$IS_TESTNET" ]]; then
  /usr/local/bin/pulltestnetscripts.sh
else
  /usr/local/bin/startpaaszapatad.sh
fi

#!/bin/bash
# TODO: run as another user, security, system settings
# POST-LAUNCH TODO: This should probably not be reading files directly from git, or if so, from versioned releases. 

# UPDATE YOUR DEBIAN 10 SYSTEM TO THE LATEST VERSIONS OF PACKAGES
apt update
apt upgrade -y

# CREATE ZAPATA USER
useradd -m -d /zapata zapata
useradd -m -d /ipfs ipfs

# INSTALL UNZIP AND WGET
apt install -y unzip wget libbz2-dev libsnappy-dev libncurses5-dev libreadline-dev

# FILESYSTEM LIMITS AS ADVISED HERE: https://developers.steem.io/tutorials-recipes/exchange_node
echo "*      hard    nofile     94000" >> /etc/security/limits.conf
echo "*      soft    nofile     94000" >> /etc/security/limits.conf
echo "*      hard    nproc      64000" >> /etc/security/limits.conf
echo "*      soft    nproc      64000" >> /etc/security/limits.conf
echo "fs.file-max = 2097152" >> /etc/sysctl.conf
sysctl -p

# INSTALL IPFS
wget https://dist.ipfs.io/go-ipfs/v0.6.0/go-ipfs_v0.6.0_linux-amd64.tar.gz
tar -xvzf go-ipfs_v0.6.0_linux-amd64.tar.gz
cd go-ipfs
sudo bash install.sh
cd ..
rm -rf go-ipfs

#INITALIZAE IPFS (NOTE: DO WE HAVE TO DO THIS AS THE IPFS USER ALSO?)
ipfs init
nohup ipfs daemon &
sleep 10

# INSTALL IPFS SYSTEMD SERVICE
ipfs get -o /etc/systemd/system/ipfs-hardened.service QmNQPATMBjfuLTmkScWAsogcPLPtPcR2goadb6tRPgEsaW


# DOWNLOAD BUILD ARTIFACTS
# POST-LAUNCH TODO: THIS SHOULD GET SOME KIND OF "LATEST" VERSION.  
# POST-LAUNCH TODO: CI SYSTEM SHOULD RELEASE ZAPATAD AND CLI_WALLET TO IPFS
# QmT6B3h88jYkEfsJCxwQQzrW1VVoido26mwJDHuiLHrciH is the build artifacts
ipfs get -o download QmT6B3h88jYkEfsJCxwQQzrW1VVoido26mwJDHuiLHrciH
# wget https://gitlab.com/zapata/zapata/-/jobs/596005137/artifacts/download

# UNZIP THE BUILD ARTIFACTS, ZAPATAD AND CLI_WALLET
unzip download

# PUT ZAPATAD AND CLI_WALLET ON YOUR $PATH
mv build/programs/zapatad/zapatad_witness /usr/bin/zapatad
mv build/programs/cli_wallet/cli_wallet /usr/bin/cli_wallet
rm -rf build
rm download

# ENSURE THAT ZAPATAD AND CLI_WALLET ARE EXECUTABLE
chmod +x /usr/bin/zapatad
chmod +x /usr/bin/cli_wallet

# IMPORT 1.3 MILLION STEEM ACCOUNTS AND CONFIGURATION TEMPLATE
# testnet snaphsot.json is QmU2zT7W2GbifQxqpU9ALMNFUT2QwsBt4L7SaHpm6QTm4Q
ipfs get -o /zapata/snapshot.json QmU2zT7W2GbifQxqpU9ALMNFUT2QwsBt4L7SaHpm6QTm4Q
ipfs pin add QmU2zT7W2GbifQxqpU9ALMNFUT2QwsBt4L7SaHpm6QTm4Q
# wget -O /zapata/snapshot.json https://test.zapata.world/_download/snapshot.json

# witness_config.ini is QmX5n6nVhbEKUMvgJre74wNdP7Jcq4GJRdw7G9BZF3zxnU
ipfs get -o /zapata/config.ini QmX5n6nVhbEKUMvgJre74wNdP7Jcq4GJRdw7G9BZF3zxnU
ipfs pin add QmX5n6nVhbEKUMvgJre74wNdP7Jcq4GJRdw7G9BZF3zxnU
# wget -O /zapata/config.ini https://gitlab.com/zapata/zapata/-/raw/dev/doc/witness_config.ini

# INSTALL ZAPATAD.SERVICE
# QmVeeCuWM6tdWxML7yEFfpaqZN9f4TL1WMd7wGgkp35Npz
# wget -O /etc/systemd/system/zapatad.service https://gitlab.com/zapata/zapata/-/raw/dev/doc/zapatad.service
ipfs get -o /etc/systemd/system/zapatad.service QmVeeCuWM6tdWxML7yEFfpaqZN9f4TL1WMd7wGgkp35Npz
ipfs pin add QmVeeCuWM6tdWxML7yEFfpaqZN9f4TL1WMd7wGgkp35Npz

# ENABLE ZAPATAD SYSTEMD SERVICE
systemctl enable zapatad

# START ZAPATAD
systemctl start zapatad

# ENABLE IPFS SYSTEMD SERVICE
systemctl enable ipfs-hardened.service

# CANNOT CURRENTLY START THIS SERVICE, IT WILL FAIL BECAUSE OF IPFS DAEMON
# systemctl start ipfs-hardened.serrvice
